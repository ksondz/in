<?php

namespace In\Charts;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class ChartsController extends Controller {


	/**
	 * Get params for draw chart.
	 *
	 * @return array
	 */
	public function getEventsChartData(){

		$events_config = Config::get('charts::events');
		$expire = strtotime(date('Y-m-d')) + (86400 * 5);

		$post = Input::all();

		$events_request_data = array(
			'api_key' => $events_config['api_key'],
			'event' => '["Survey Start","Social Share","Signup"]',
			'expire' => $expire,
			'type' => $post['type'] ? $post['type'] : $events_config['default_type'],
			'unit' => $post['unit'] ? $post['unit'] : $events_config['default_unit'],
			'interval' => $events_config['default_interval']
		);


		$period_dates = array(
			'end_period' => strtotime($post['end_date']),
			'now' => strtotime(date('Y-m-d')),
			'interval_params' => date_diff(new \DateTime($post['start_date']), new \DateTime()),
			'show_interval_params' => date_diff(new \DateTime($post['start_date']), new \DateTime($post['end_date']))
		);


		$chart_data = array(
			'count_items' => 7,
			'date_label_format' => '%b %e',
			'date_tooltip_format' => '%b %e',
			'chart_data' => array()
		);

		switch ($events_request_data['unit']) {
			case 'hour':

				$events_request_data['interval'] = (int) (( $period_dates['interval_params']->days * 24 ) + $period_dates['interval_params']->format('%h')) - 7;

				$chart_data['count_items'] = (int) ($period_dates['now'] - $period_dates['end_period']) != 0 ?
					(($period_dates['show_interval_params']->days * 24) + $period_dates['show_interval_params']->format('%h') + 24 ) :
					$events_request_data['interval'];

				$chart_data['date_label_format'] = '%m/%e %l%p';
				$chart_data['date_tooltip_format'] = '%l%p, %a, %b %e';

				break;

			case 'day':

				$events_request_data['interval'] = $period_dates['interval_params']->days + 1;

				$chart_data['count_items'] = (($period_dates['now'] - $period_dates['end_period']) != 0 ?
					( $period_dates['show_interval_params']->days + 1 ) :
					$events_request_data['interval']);

				$chart_data['date_tooltip_format'] = '%a, %b %e';

				break;

			case 'week':

				$events_request_data['interval'] = (int)(( $period_dates['interval_params']->days / 7 ) + 1);

				$chart_data['count_items'] = (int) (($period_dates['now'] - $period_dates['end_period']) != 0 ?
					( $period_dates['show_interval_params']->days / 7 ) + 1 :
					$events_request_data['interval']);

				break;

			case 'month':

				$interval_month = $period_dates['interval_params']->format('%y') ?
					( $period_dates['interval_params']->format('%y') * 12 ) + $period_dates['interval_params']->format('%m') :
					$period_dates['interval_params']->format('%m');

				$show_interval_month = $period_dates['show_interval_params']->format('%y') ?
					( $period_dates['show_interval_params']->format('%y') * 12 ) + $period_dates['show_interval_params']->format('%m') :
					$period_dates['show_interval_params']->format('%m');

				$events_request_data['interval'] = (int) $interval_month + 1;

				$chart_data['count_items'] = (int) (($period_dates['now'] - $period_dates['end_period']) != 0 ?
					( $show_interval_month + 1 )  :
					$events_request_data['interval']);

				$chart_data['date_tooltip_format'] = '%B, %Y';

				break;
		}

		$signature = $this->getSign($events_request_data ,$events_config['api_secret']);
		$chart_data['chart_data'] = $this->getEvents($events_request_data,$signature);

        return $chart_data;

    }


	/**
	 * Sent request to mix panel and get events with data
	 *
	 * @param $events_request_data
	 * @param $signature
	 * @return mixed
	 */
	private function getEvents($events_request_data,$signature){

		$events_url_string = Config::get('charts::events.url').'?';

		foreach($events_request_data as $key => $val ){
			$events_url_string .= $key.'='.$val.'&';
		}

		$events_url_string .= 'sig='.$signature;

		$request_send = \Requests::get($events_url_string);

		return json_decode($request_send->body);

	}


	/**
	 * Generate signature
	 */
	private function getSign($signatureData,$api_secret){

        ksort($signatureData);

        $args_concat = '';

        foreach($signatureData as $key=>$val){
            $args_concat .= $key.'='.$val;
        }

		return md5($args_concat.$api_secret);
    }


}