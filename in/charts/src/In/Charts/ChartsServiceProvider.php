<?php

namespace In\Charts;

use Illuminate\Support\ServiceProvider;
use In\Charts\WidgetException as Exception;

class ChartsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('in/charts');

		include __DIR__.'/../../routes.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

		$this->app['widget'] = $this->app->share(function($app)
		{
			return new Widget;
		});
		$this->app->booting(function()
		{
			$loader = \Illuminate\Foundation\AliasLoader::getInstance();
			$loader->alias('Widget', 'In\Charts\Facades\Widget');

			$widgetFile = __DIR__.'/../../widgets.php';

			if(file_exists($widgetFile))
			{
				include $widgetFile;
			}else
			{
				throw new Exception("Widget file not found!");
			}
		});

		

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
