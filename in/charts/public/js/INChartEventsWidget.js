var INChartEventsWidget = (function() {

    var INChartEventsWidget = function(widgetContainer) {
        this.widgetContainer = widgetContainer;
    };

    INChartEventsWidget.prototype.render = function(chartData) {
        var allCategories = chartData.data.series;
        var allValues = chartData.data.values;
        var signupParams = {
                title : 'Signup',
                values : []
            },
            socialShareParams = {
                title : 'Social Share',
                values : []
            },
            surveyStartParams = {
                title : 'Survey Start',
                values : []
            },
            categories = [];

        var type = $(".graph-menu-filters .filter-unit").find(":selected").attr('data-filter');

        for(var i = 0; i < allCategories.length; i++) {

            if(i < chartData.countItems) {

                var reg = /(\d+)-(\d+)-(\d+)/;

                var categoryDate,categoryDateParts;

                if ( type == 'hour' ) {
                    reg = /(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/;
                    categoryDateParts = reg.exec(allCategories[i]);
                    categoryDate = Date.UTC(
                        categoryDateParts[1],
                        categoryDateParts[2] - 1,
                        categoryDateParts[3],
                        categoryDateParts[4],
                        categoryDateParts[5],
                        categoryDateParts[6]
                    );
                } else {
                    categoryDateParts = reg.exec(allCategories[i]);
                    categoryDate = Date.UTC(categoryDateParts[1],categoryDateParts[2] - 1,categoryDateParts[3]);
                }

                categories.push(categoryDate);
                signupParams.values.push([categoryDate,allValues['Signup'][allCategories[i]]]);
                socialShareParams.values.push([categoryDate,allValues['Social Share'][allCategories[i]]]);
                surveyStartParams.values.push([categoryDate,allValues['Survey Start'][allCategories[i]]]);
            }
        }

        var lineStyle = chartData.countItems < 3 ? 'dash' : 'line';

        var lastLabel = '';

        var container = this.widgetContainer.find('.chart-container').get(0);

        var options = {
            chart: {
                renderTo: container,
                height: 351,
                type: 'line'
            },
            title: {
                text: '',
                x: -20 //center
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                lineColor: '#000',
                lineWidth: 1,

                type: 'datetime',

                labels: {
                    formatter: function() {
                        var label = Highcharts.dateFormat(chartData.dateLabelFormat ? chartData.dateLabelFormat : '%b. %e', this.value);

                        if (lastLabel != label) {
                            lastLabel = label;
                            return label;

                        } else {
                            return '';
                        }
                    }
                },
                showFirstLabel: true,
                showLastLabel: true
            },
            exporting: {
                enabled: false
            },
            yAxis: {
                title: {
                    text: 'Total visitors'
                },
                min : 0,
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }],
                startOnTick: false
            },
            colors: ['#A47D7C', '#3D96AE', '#80699B'],
            tooltip: {
                valueSuffix: '',
                borderRadius: 5,
                useHTML: true,
                xDateFormat: chartData.dateTooltipFormat ? chartData.dateTooltipFormat : '%b. %e',
                headerFormat: '<span style="color: {series.color}">{series.name}</span><br><span style="color:#606060">{point.key}: {point.y}</span>',
                pointFormat: ''
            },
            legend: {
                borderRadius: 5,
                borderWidth: 1
            },
            series: [{
                name: surveyStartParams.title,
                data: surveyStartParams.values,
                dashStyle: lineStyle
            }, {
                name: socialShareParams.title,
                data: socialShareParams.values,
                dashStyle: lineStyle
            },{
                name: signupParams.title,
                data: signupParams.values,
                dashStyle: lineStyle
            } ],
            plotOptions: {
                series: {
                    marker: {
                        enabled: true
                    }
                }
            }

        };

        var chart = new Highcharts.Chart(options);

        $(window).resize(function() {
            chart.redraw();
        });
    }

    INChartEventsWidget.prototype.draw = function() {
        var widget = this;

        $.ajax({
            url : '/chart/getEventsChartData',
            type : 'POST',
            data : {
                start_date  : widget.widgetContainer.attr('data-format-calendar-start-value'),
                end_date    : widget.widgetContainer.attr('data-format-calendar-end-value'),
                type : widget.widgetContainer.find(".graph-menu-filters .filter-type").find(":selected").attr('data-filter'),
                unit : widget.widgetContainer.find(".graph-menu-filters .filter-unit").find(":selected").attr('data-filter')
            },
            success:function(params){

                if (params) {

                    if (params.chart_data.legend_size != 0) {

                        widget.widgetContainer.find('.nodata').hide();
                        widget.widgetContainer.find('.container').show();

                        var chartData = {
                            data : params.chart_data.data,
                            countItems : params['count_items'],
                            dateLabelFormat : params['date_label_format'],
                            dateTooltipFormat : params['date_tooltip_format']
                        };

                        widget.render(chartData);
                    } else {
                        widget.widgetContainer.find('.container').hide();

                        var noDataMessage = "Sorry, we don't have any data for this time period yet. Please try again later.";
                        widget.widgetContainer.find('.nodata').text(noDataMessage).show()
                    }
                }
            }
        });
    }

    return INChartEventsWidget;

})();