$( document ).ready(function() {

	$('.events-chart-box').each(function(){

		var widgetContainer = $(this);



		INChartsDatePeriod.initChartPeriodDatepicker(widgetContainer);

        var chart = new INChartEventsWidget(widgetContainer);
        chart.draw();


        widgetContainer.find(".date-picker-close").click(function(){
            widgetContainer.find( ".datepicker" ).hide();
            $(this).hide();

            var periodBtn = widgetContainer.find('.period-btn');

            if(periodBtn.attr('data-state') == 'change'){

                chart.draw();
                periodBtn.removeAttr('data-state');
            }

        });

		widgetContainer.find(".graph-menu-filters select").on('change',function(){

			var datePicker = widgetContainer.find( ".datepicker");

			if(datePicker.css("display") == 'block'){
				datePicker.hide();
				widgetContainer.find(".date-picker-close").hide();
			}

            chart.draw();

		});

	});

});


